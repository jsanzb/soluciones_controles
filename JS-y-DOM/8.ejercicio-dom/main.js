// # Ejercicio 8
// Crea un cronómetro que permita ponerlo en marcha, pararlo y resetearlo.

'use strict';

let interval;
let runDate;
let contador;

function parseTime(runDif) {
  let mSeconds = runDif;
  let vMseconds = (runDif + 1000 + '').slice(-3);
  let vSeconds = Math.floor((mSeconds % 60000) / 1000);
  let vMinutes = Math.floor((mSeconds % 3600000) / 60000);
  let vHours = Math.floor(mSeconds / 3600000);
  if (vSeconds < 10) vSeconds = '0' + vSeconds;
  if (vMinutes < 10) vMinutes = '0' + vMinutes;
  if (vHours < 10) vHours = '0' + vHours;
  return `${vHours}:${vMinutes}:${vSeconds}.${vMseconds}`;
}

function iniciarTimer() {
  const nowDate = new Date();
  let runDif = nowDate.getTime() - runDate.getTime();
  contador.innerHTML = parseTime(runDif);
}

const buttonIniciar = document.querySelector('.Iniciar');
const handleClickIniciar = (event) => {
  contador = document.querySelector('h2');
  runDate = new Date();
  interval = setInterval(iniciarTimer, 1);
  buttonIniciar.disabled = true;
  buttonParar.disabled = false;
};
buttonIniciar.addEventListener('click', handleClickIniciar);

const buttonParar = document.querySelector('.Parar');
const handleClickParar = (event) => {
  clearInterval(interval);
  buttonIniciar.disabled = false;
  buttonParar.disabled = true;
};
buttonParar.addEventListener('click', handleClickParar);

const buttonResetear = document.querySelector('.Resetear');
const handleClickResetear = (event) => {
  contador.innerHTML = '00:00:00.000';
  clearInterval(interval);
  runDate = '';
  buttonIniciar.disabled = false;
  buttonParar.disabled = true;
};
buttonResetear.addEventListener('click', handleClickResetear);
