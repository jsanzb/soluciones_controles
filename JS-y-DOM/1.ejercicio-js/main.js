// # Ejercicio 1

// Escribe una función que devuelva un array de usuarios. De cada usuario guardaremos el username, el nombre y apellido, el sexo, el país,
// el email y un enlace a su foto de perfil.

// El número de usuarios a obtener se debe indicar con un parámetro de dicha función.

// Sírvete de la API: https://randomuser.me/

'use strict';

const people = [];

async function getCharacters(number) {
  const response = await fetch(`https://randomuser.me/api/?results=${number}`);
  const data = await response.json();
  try {
    people.length = 0;
    for (const person of data.results) {
      const onePerson = {};
      onePerson.username = person.login.username;
      onePerson.first = person.name.first;
      onePerson.last = person.name.last;
      onePerson.gender = person.gender;
      onePerson.email = person.email;
      onePerson.large = person.picture.large;
      people.push(onePerson);
    }
    console.log(people);
  } catch (error) {
    console.log('Error en la consulta.');
  }
}

getCharacters(5);
