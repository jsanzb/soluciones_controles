// # Ejercicio 7
// Crea una malla de cuadrados de tal forma que el color de cada uno de ellos se
// determine de forma aleatoria y vaya cambiando cada segundo.
// ![Ejemlo](exemplo.png)
// Añade también un botón que permita añadir un nuevo cuadrado a la maya con las
// mismas propiedades que los anteriores.

'use strict';

function getRandom(max) {
  return Math.round(Math.random() * max);
}

function changeColor(objeto) {
  for (const boxChilds of box[0].children) {
    boxChilds.style.backgroundColor = `rgb(${getRandom(255)},${getRandom(
      255
    )},${getRandom(255)})`;
  }
}

const button = document.querySelector('button');
const handleClick = (event) => {
  const cajaDiv = document.querySelector('.caja');
  const newDiv = document.createElement('div');
  cajaDiv.append(newDiv);
  changeColor(box);
};
button.addEventListener('click', handleClick);

const box = document.getElementsByClassName('caja');
changeColor(box);

setInterval(() => {
  changeColor(box);
}, 1000);
