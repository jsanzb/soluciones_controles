// # Ejercicio 2
// Crea un programa que imprima cada 5 segundos el tiempo desde la ejecución del mismo.
// Formatea el tiempo para que se muestren los segundos, los minutos, las horas y los días desde la ejecución.

'use strict';

const runDate = new Date();
const runDateseconds = runDate.getTime();
console.log(`
Fecha de ejecucion
${runDate}
====================

Tiempo transcurrido
`);

function dateval() {
  const nowDate = new Date();
  let runDif = nowDate.getTime() - runDate.getTime();
  let runSeconds = Math.floor(runDif / 1000);
  let runMinutes = Math.floor(runDif / (1000 * 60));
  let runHours = Math.floor(runDif / (1000 * 60 * 60));
  let runDays = Math.floor(runDif / (1000 * 60 * 60 * 24));
  console.log(
    `${runSeconds} Segundos, ${runMinutes} Minutos, ${runHours} Horas, ${runDays} Dias
  ${nowDate}`
  );
}

setInterval(() => {
  dateval();
}, 5000);
