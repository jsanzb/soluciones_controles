// # Ejercicio 4
// Escribe una función que, al recibir un array como parámetro, elimine los strings repetidos del mismo.
// No se permite hacer uso de Set ni Array.from().

'use strict';

const names = [
  'A-Jay',
  'Manuel',
  'Manuel',
  'Eddie',
  'A-Jay',
  'Su',
  'Reean',
  'Manuel',
  'A-Jay',
  'Zacharie',
  'Zacharie',
  'Tyra',
  'Rishi',
  'Arun',
  'Kenton',
];

const namesOut = [];

function namesReduce(names) {
  for (const name of names) {
    let check = 0;
    for (const nameout of namesOut) {
      if (name === nameout) {
        check++;
      }
    }
    if (check === 0) {
      namesOut.push(name);
    }
  }
  console.log(names);
  console.log(namesOut);
}

namesReduce(names);
