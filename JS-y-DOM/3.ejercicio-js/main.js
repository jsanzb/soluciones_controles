// # Ejercicio 3
// Crea un programa que reciba un número en decimal o binario y devuelva la conversión:
// -   Si le pasamos un nº en decimal debe retornar la conversión a binario.
// -   Si le pasamos un nº en binario deberá devolver su equivalente decimal.
// Para ello la función deberá recibir un segundo parámetro que indique la base: 10 (decimal) o 2 (binario).
// No se permite utilizar el método parseInt().

'use strict';

let valorBinTemp;

function conversor(valor, base) {
  let valorTemp = valor;
  if (base === 10) {
    let valorBin = '';
    while (valorTemp > 1) {
      valorBinTemp = valorTemp % 2;
      valorTemp = Math.floor(valorTemp / 2);
      valorBin = valorBinTemp + valorBin;
    }
    valorBin = valorTemp + valorBin;
    return `${valor} (Decimal) = ${valorBin} (Binario)`;
  } else if (base === 2) {
    const valorString = valor + '';
    const longValor = (valor + '').length;
    let valorPosicion;
    let potBase = 1;
    let valorDec = 0;
    for (let index = longValor; index > 0; index--) {
      valorPosicion = valorString.charAt(index - 1);
      valorDec = valorDec + valorPosicion * potBase;
      potBase = potBase * 2;
    }
    return `${valor} (Binario) = ${valorDec} (Decimal)`;
  } else {
    return 'La base de la conversion debe ser 2 o 10';
  }
}

console.log(conversor(10876, 10));
console.log(conversor(10101001111100, 2));
