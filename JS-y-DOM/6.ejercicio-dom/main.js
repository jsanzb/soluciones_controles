// # Ejercicio 6
// Subraya (manipulando el DOM) todas las palabras de los párrafos en "ejercicio.html"
//  que contengan más de 5 letras.

'use strict';

const p1 = document.querySelector('body > p:nth-child(1)');
const p2 = document.querySelector('body > p:nth-child(2)');
const p3 = document.querySelector('body > p:nth-child(3)');

function parse(text) {
  let textOut = text;
  let textArray = text.split(/[., "-]/);
  for (let index = 0; index < textArray.length; index++) {
    if (textArray[index].length > 5) {
      if (textOut.indexOf(`<u>${textArray[index]}</u>`) === -1) {
        textOut = textOut.replaceAll(
          textArray[index],
          `<u>${textArray[index]}</u>`
        );
      }
    }
  }
  return textOut;
}

p1.innerHTML = parse(p1.innerHTML);
p2.innerHTML = parse(p2.innerHTML);
p3.innerHTML = parse(p3.innerHTML);
