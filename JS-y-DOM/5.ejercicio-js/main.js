// # Ejercicio 5
// Consigue una lista con los nombres de los personajes de la serie Rick and Morty
// que aparecen en los episodios lanzados en el mes de enero (el año no importa).
// Utiliza llamadas a la API: 'https://rickandmortyapi.com/api/'

'use strict';

const people = [];

async function getCharacters() {
  const response = await fetch('https://rickandmortyapi.com/api/episode');
  const data = await response.json();
  try {
    for (const episode of data.results) {
      if (episode.air_date.includes('January')) {
        console.log(
          `Episodio "${episode.name}" emitido en ${episode.air_date}`
        );
        console.log(' Personajes:');
        for (const character of episode.characters) {
          const responseCharacter = await fetch(character);
          const dataCharacter = await responseCharacter.json();
          try {
            console.log(`   ${dataCharacter.name} (${dataCharacter.species})`);
          } catch (error) {
            console.log('Error en la consulta de Personajes.');
          }
        }
      }
    }
  } catch (error) {
    console.log('Error en la consulta de episodios.');
  }
}

getCharacters();
